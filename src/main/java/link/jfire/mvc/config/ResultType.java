package link.jfire.mvc.config;

public enum ResultType
{
    Json, Beetl, FreeMakrer, Jsp, Html, Redirect, None,String,Bytes
}

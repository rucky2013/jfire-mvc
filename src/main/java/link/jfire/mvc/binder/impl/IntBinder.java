package link.jfire.mvc.binder.impl;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import link.jfire.baseutil.StringUtil;
import link.jfire.baseutil.verify.Verify;
import link.jfire.mvc.binder.AbstractDataBinder;

public class IntBinder extends AbstractDataBinder
{
    
    public IntBinder(String paramName)
    {
        super(paramName);
    }
    
    @Override
    public Object binder(HttpServletRequest request, Map<String, String> map, HttpServletResponse response)
    {
        String value = map.get(paramName);
        Verify.True(StringUtil.isNotBlank(value), "参数为int基本类型，页面必须要有传参，请检查传参名字是否是{}", paramName);
        return Integer.valueOf(value);
    }
    
}

package link.jfire.mvc.binder.impl;

import link.jfire.mvc.binder.DataBinder;

/**
 * 这是一个类型接口，用来表示是http的绑定器
 * 
 * @author 林斌
 * 
 */
public interface HttpBinder extends DataBinder
{
    
}
